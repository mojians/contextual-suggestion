# -*- coding: utf-8 -*-

# Scrapy settings for immo project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'immo'

SPIDER_MODULES = ['immo.spiders']
NEWSPIDER_MODULE = 'immo.spiders'

AUTOTHROTTLE_ENABLED = False
DOWNLOAD_DELAY = 0.5

#ITEM_PIPELINES = ['immo.pipelines.ImmoPipeline']

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'immo (+http://www.yourdomain.com)'
