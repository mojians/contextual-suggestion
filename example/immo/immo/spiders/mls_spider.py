#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import scrapy
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.http import FormRequest, Request
from immo.items import ImmoItem
import re
import unicodedata
import json

import scrapy
from immo.items import MlsItem

class MlsSpider(scrapy.Spider):
    name = 'mls'
    allowed_domains = ['realtor.ca']
    start_urls = [
        "http://realtor.ca",
    ]
    currentPage = '1'
    numlistings = 0
    

    def parse(self, response):
      yield FormRequest(url="http://realtor.ca/api/Listing.svc/PropertySearch_Post",
                    formdata={'CultureId':'2',
          'ApplicationId':'1',
          'RecordsPerPage':'300',
          'MaximumResults':'300',
          'PropertyTypeId':'300',
          'TransactionTypeId':'2',
          'SortOrder':'A',
          'SortBy':'1',
          'LongitudeMin':'-127.9248046875',
          'LongitudeMax':'-45.0439453125',
          'LatitudeMin':'52.93539577328555',
          'LatitudeMax':'63.37183160842689',
          'PriceMin':'0',
          'PriceMax':'0',
          'BedRange':'0-0',
          'BathRange':'0-0',
          'ParkingSpaceRange':'0-0',
          'viewState':'l',
          'CurrentPage':self.currentPage},
                    callback=self.parse_post)

      
    def parse_post(self, response):
      json_string = response.body
      json_dict = json.loads(json_string)
      
      for listing in json_dict['Results']:
        item = MlsItem()
        item['row'] = listing
        yield item
      
      currentPageInt = int(self.currentPage) + 1
      self.currentPage = str(currentPageInt)
      
      numListings = int(json_dict['Paging']['TotalRecords'])
      
      if int(self.currentPage) * 300 <= numListings:
      
        yield FormRequest(url="http://realtor.ca/api/Listing.svc/PropertySearch_Post",
                      formdata={'CultureId':'2',
            'ApplicationId':'1',
            'RecordsPerPage':'300',
            'MaximumResults':'300',
            'PropertyTypeId':'300',
            'TransactionTypeId':'2',
            'SortOrder':'A',
            'SortBy':'1',
            'LongitudeMin':'-135.5712890625',
            'LongitudeMax':'-37.3974609375',
            'LatitudeMin':'40.78054031957272',
            'LatitudeMax':'70.3483170658498',
            'PriceMin':'0',
            'PriceMax':'0',
            'BedRange':'0-0',
            'BathRange':'0-0',
            'ParkingSpaceRange':'0-0',
            'viewState':'l',
            'CurrentPage':self.currentPage},
                      callback=self.parse_post)
