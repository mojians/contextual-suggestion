#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import scrapy
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.http import FormRequest, Request
from immo.items import ImmoItem
import re
import unicodedata

class ImmoSpider(CrawlSpider):
    name = "immo"
    allowed_domains = ["duproprio.com"]
    base_url = "http://duproprio.com"
    start_urls = [
        #"http://duproprio.com/search/?hash=/g-pr=1/s-pmin=0/s-pmax=99999999/s-build=1/s-days=0/s-filter=forsale/p-con=main/p-ord=date/p-dir=DESC/pa-ge=1/",
        "http://duproprio.com"
        ]

    rules = (
        #Rule(SgmlLinkExtractor(allow=(r'\/maison-a-vendre.*')), callback='parse_house', follow=True),
    )

    def parse_start_url(self, response):
        yield FormRequest.from_response(response, formxpath='//*[@id="searchBar"]/form', callback=self.parse_listings)


    def parse_house(self, response):
        item = ImmoItem()
        row = dict()

        listing_type = 'undefined'
        type_reg = re.compile('.*(maison|condo|jumele|chalet).*')
        if(type_reg.match(response.url)):
            m = re.search(type_reg, response.url)
            listing_type = m.group(1)

        listing_id = response.xpath('//*[@id="listingContent"]/h3/text()[1]').extract()
        listing_id = listing_id[0]
        listing_id = int(listing_id[1:])

        price = response.xpath('//*[@id="listingContent"]/h2/text()').extract()
        price = price[0].strip(' \t\n\r')

        street_address = response.xpath('//*[@id="details"]/div[2]/p/strong/span[1]/text()').extract()
        street_address = street_address[0].strip(' \t\n\r')

        locality = response.xpath('//*[@id="listingContent"]/h1/div/div/meta[1]/@content').extract()
        locality = locality[0]
        region = response.xpath('//*[@id="listingContent"]/h1/div/div/meta[2]/@content').extract()
        region = region[0]

        lat = response.xpath('//*[@id="miniMapListingWrapper"]/@data-lat').extract()
        lat = lat[0]
        lon = response.xpath('//*[@id="miniMapListingWrapper"]/@data-lat').extract()
        lon = lon[0]

        item['listing_id'] = listing_id
        item['listing_type'] = listing_type
        item['price'] = price
        item['street_address'] = self.clean_string(street_address)
        item['locality'] = self.clean_string(locality)
        item['region'] = self.clean_string(region)

        for sel in response.xpath('//*[@id="details"]/div[2]/div[1]/ul/li'):
            row_col = sel.xpath('strong/text()').extract()
            row_value = sel.xpath('text()').extract()
            row[self.clean_string(row_col[0][:-2])] = self.clean_string(row_value[0][1:])

        item['specs'] = row
        item['url'] = response.url
        yield item

    def parse_listings(self, response):

        links = response.xpath('//a')
        for link in links:
            url = link.xpath('./@href').extract()
            relevant_url = re.compile('\/(maison|condo|jumele|chalet)-a-vendre.*')
            if(relevant_url.match(url[0])):
                yield Request(self.base_url + url[0], self.parse_house)

        next_link = response.xpath('//*[@id="searchNav"]/ul/li[@class="next"]')
        if next_link:
            link = next_link.xpath('a/@href').extract()
            link = self.base_url + link[0]
            yield Request(link, self.parse_listings)

    def remove_accents(self, s):
        return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))

    def escape_double_quotes(self, s):
        return s.replace('"', '\\"')

    def remove_exposants(self, s):
        return s.replace('\xb2', '2')

    def clean_string(self, s):
        x = self.remove_accents(s)
        y = self.escape_double_quotes(x)
        z = self.remove_exposants(y)
        return z
