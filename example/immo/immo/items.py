import scrapy

class ImmoItem(scrapy.Item):
    listing_id = scrapy.Field()
    listing_type = scrapy.Field()
    price = scrapy.Field()
    url = scrapy.Field()
    street_address = scrapy.Field()
    locality = scrapy.Field()
    region = scrapy.Field()
    specs = scrapy.Field()
    lat = scrapy.Field()
    lon = scrapy.Field()
    
class MlsItem(scrapy.Item):
  row = scrapy.Field()
