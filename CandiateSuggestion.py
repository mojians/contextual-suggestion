__author__ = 'Jian Mo'

from Search import SearchBy, GoogleUrl,getReviews
import json
import time

context = "42.12922,-80.08506"

def getCandidateSuggestion(context,upbound):

    candidateID=[]
    page=1
    while len(candidateID)<upbound:
        if page==1:
            searchResult = SearchBy(GoogleUrl, location=context,hasNextPage="true",radius=500)
        else:
            searchResult = SearchBy(GoogleUrl, location=context,hasNextPage="true",radius=500,pagetoken=next_page_token)
        jsonDict=json.loads(searchResult)
        #print searchResult

        for resultDict in jsonDict["results"]:
            candidateID.append(resultDict["place_id"])
        page=page+1
        #print searchResult
        try:
            next_page_token=jsonDict["next_page_token"]
        except:
            return candidateID
        time.sleep(2)

    return candidateID

def candidateRep(candidateList):
    candidateReviewList=[]
    n=1
    for candidate in candidateList:
        print n
        n=n+1
        negative=getReviews(candidate,False).encode('ascii', 'ignore')
        positive=getReviews(candidate,True).encode('ascii', 'ignore')
        with open("H:/WareHouse/TREC/Contextual Suggestion/data/positive/"+candidate+"_positive.txt","a") as text:
            text.write(positive)
        with open("H:/WareHouse/TREC/Contextual Suggestion/data/negative/"+candidate+"_negative.txt","a") as text:
            text.write(negative)
        candidateReviewList.append({"candidateID":candidate,"negative":negative,"positive":positive})

    return candidateReviewList



if __name__ == '__main__':
    #print getCandidateSuggestion(context,100)
    with open("candidateReviewList.txt","w") as text:
        text.write(str(candidateRep(getCandidateSuggestion(context,200))))
