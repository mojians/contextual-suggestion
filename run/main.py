__author__ = 'Jian Mo'
from flask import Flask, jsonify, request, abort,redirect, url_for
import random, sqlite3, jsonschema, json
import pystache
import csv
import learn2rank as rank
import MySQLdb
from elastic import elastic_rank
from newrank import score_user_profile,svm_rank,suppressor
import nltk.metrics as metric
import datetime

app = Flask(__name__)
client = app.test_client()

conn = MySQLdb.connect(user='root',passwd='3231862',db='new_docs',
        host='localhost',
        charset="utf8",
        use_unicode=True,
        )
cur = conn.cursor()

#@app.route('/suggestions', methods=['POST'])
def process_post(request):
    print "processing post"
    rdata=request.get_json()
    # with open("request_schema.json") as f:
    #     schema = json.loads(f.read())
    #     print(schema)
    #print "attempting"
    print "data is ",type(rdata)

    # TODO: Replace this with a more intelligent recommendation strategy
    ids = get_ids_for_location(rdata)
    #print "ids is ",ids
    if ids != []:
        suggestions = ids[0:49]
        return jsonify({'suggestions': suggestions})
    else:
        return "place id is not valid"


@app.route('/', methods=["GET","POST"])
def index():
    if request.method=="POST":
        print "post"
        #print "request=",request.data
        form_name = request.form.get('location', None)

        #print "form_name=",form_name
        if form_name is not None:
            print "posted form"

            rdata=form_request_parser(request.form)
            rdata=json.loads(rdata)
            ids=get_ids_for_location(rdata)
            suggestions = ids[0:10]
            location=rdata["location"]["id"]
            rows=get_id_info(suggestions)
        else:
            print "process_post"
            return process_post(request)

    if request.method=="GET":
        print "get"
        rows=[]
        tags=[]
        location=""

    # with open('render/tags.csv') as f:
    #     reader = csv.reader(f)
    #     tags = [row[1] for row in reader]

    tags=""
    with open('render/templates/suggestions.html') as f:
        template = f.read()
        local_url=request.url
        # print template
        data = {'tags': tags, 'raw_tags': json.dumps(tags), 'documents': rows,"local_url":local_url,"location":location}
        # print data
        h = pystache.render(
            template, data).encode("utf-8")
        # print h
        return h


def form_request_parser(formdata):
    form=formdata
   # print request
    preferences_list=[]
    for i in range(0,10):
        documentId=form.get("item%i_docid"%i)
        rating=form.get("item%i_rating"%i,0,int)
        tags=form.getlist("item%i_tags"%i)
        item={"documentId":documentId,"rating":rating,"tags":tags}
        person={"preferences":[{}]}
        preferences_list.append(item)
    location=form.get("location")
    lng=""
    lat=""
    state=""
    name=""
    person_id=1
    duration=str(form.get("duration"))
    season=str(form.get("season"))
    group=str(form.get("group"))
    type=str(form.get("type"))

    request_dict={"location": {"lng":lng, "state": "MI", "id": location, "lat":lat, "name":name}, "person": {"preferences":preferences_list,"id": person_id}, "duration": duration, "season": season, "group": group,"type":type}
    #print  request_dict
    return json.dumps(request_dict)

# main recommendation funciton
def get_ids_for_location(data):
    #print data
    location=data["location"]["id"]
    print "location=",location
    user_rating=[]
    user_tags=[]
    site_ids=[]
    try:
        datatype=data["type"]
    except:
        datatype=""

    other_info=data["duration"]+","+data["season"]+",Good for "+data["group"]+","+datatype
    for preference in data["person"]["preferences"]:
        user_rating.append(preference["rating"])
        try:
            user_tags.append(preference["tags"])
        except:
            pass
        #print 'preference["documentId"]=',preference["documentId"]
        site_ids.append(preference["documentId"])
    print "site_ids=",site_ids
    print "user_tags=",user_tags
    print "user_ratings=",user_rating
    print "location is ",location
    p_categories=""
    p_infos=""
    p_tags=""
    n_categories=""
    n_infos=""
    n_tags=""
    positive_ids=[]
    negative_ids=[]
    p_reviews=""
    n_reviews=""
    if site_ids[0]: # if profile not provided initiate some random places
        s=0
        for i in site_ids:
            cur.execute('select site_id from sites_info WHERE site_id =%s',(i,))
            result=cur.fetchone()
            if result:
                print "result=",result
                print "i=",i
                cur.execute('select p_reviews,n_reviews,category,business_info from sites_info where site_id = %s', (i, ))
                #print "cur._rows=",cur._rows
                info_row=cur.fetchone()

                p_reviews="" if info_row[0] is None else info_row[0]
                n_reviews="" if info_row[1] is None else info_row[1]
                category="" if info_row[2] is None else info_row[2]
                business_info="" if info_row[3] is None else info_row[3]
            else:
                p_reviews=""
                n_reviews=""
                category=""
                business_info=""

            if user_rating[s]>3:
                p_categories+=category
                p_infos+=business_info.replace('"',"").replace('"',"").replace(':',"").replace("{","").replace("}","")
                p_reviews=p_reviews
                #print user_tags[s]
                try:
                    p_tags=user_tags[s][0]
                except:
                    p_tags=""
                positive_ids.append(i)
            elif user_rating[s]<3:
                n_categories+=category
                n_reviews=n_reviews
                n_infos+=business_info.replace('"',"").replace('"',"").replace(':',"").replace("{","").replace("}","")
                try:
                    n_tags=user_tags[s][0]
                except:
                    n_tags=""
                negative_ids.append(i)

            s=s+1
        user_profile=[p_reviews,n_reviews,p_categories,p_infos,p_tags,n_categories,n_infos,n_tags,other_info,location]
        print "running elastic rank",datetime.datetime.now()
        ranked_dict=elastic_rank(p_reviews,n_reviews,p_categories,p_infos,p_tags,n_categories,n_infos,n_tags,other_info,location)
        print "elastic rank complete",datetime.datetime.now()
        scored_list=score_user_profile(site_ids,user_profile)
        #print site_ids
        user_dict=dict(zip(site_ids,scored_list))
        #print "learning",datetime.datetime.now()
        learning_score=svm_rank(user_dict,ranked_dict,user_rating)

        #print "learning complete",datetime.datetime.now()
        #print "user_rating=",user_rating

        rerank_list=[x for (y,x) in sorted(zip(learning_score,ranked_dict.keys()))]   #
        #print rerank_list
        #print "precision=",metric.precision(set(positive_ids),set(rerank_list[0:10]))
        rerank_dict=dict(zip(ranked_dict.keys(),learning_score))
        print rerank_dict

        user_score_list=[]
        user_rating_dict=dict(zip(site_ids,user_rating))
        i=0
        for key in rerank_dict.keys():

            if key in user_rating_dict.keys():
                i=i+1
                #print "match"
                #print "user_dict[key]",user_rating_dict[key]
                user_score_list.append(user_rating_dict[key])
            else:
                user_score_list.append(0)
        print "user_score_list",user_score_list
        print "learning score=",learning_score
        print "accuracy:",metric.accuracy(user_score_list,learning_score)

        return rerank_list#sorted(ranked_dict)
    else:
        cur.execute('select site_id from sites_info where city_id = %s', (str(location), ))
        rows = cur.fetchall()
        return random.sample([row[0] for row in rows],10)

def get_id_info(suggestions):
        data={'suggestions': suggestions}
        rows = []
        for index in range(len(data['suggestions'])):
            id = data['suggestions'][index]
            #print id
            cur.execute(
                "select id, title, website, city_id from docs where id = %s", (str(id),))
            #print cur.fetchone()
            info_dict={}
            row = cur.fetchone()
            #print "row=",row
            info_dict["id"]=row[0]
            info_dict["title"]=row[1]
            info_dict["url"]=row[2]
            info_dict["location"]=row[3]
            info_dict['index'] = index
            rows.append(info_dict)
        return rows




if __name__ == '__main__':
    import logging
    logging.basicConfig(filename='log/server.log',level=logging.DEBUG)
    app.run(host="0.0.0.0",debug=True, port=80)