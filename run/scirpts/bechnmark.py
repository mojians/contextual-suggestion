__author__ = 'Jian Mo'

import json
import urllib2
from validate import validate as val
import json
from nltk.metrics import precision
import MySQLdb
import pystache
import random
renderer = pystache.Renderer()


url="http://45.55.182.214/"
localhost="http://127.0.0.1/"

template=  	{
      "location": {
        "lng": -85.6681,
        "state": "",
        "id": "",
        "lat": 42.9634,
        "name": ""
      },
      "person": {
        "preferences": [

        ],
        "id": 1
      },
      "duration": "",
      "season": "",
      "group": ""
    }

conn = MySQLdb.connect(user='root',passwd='3231862',db='test_docs',
        host='localhost',
        charset="utf8",
        use_unicode=True,
        )
cur = conn.cursor()


def precision_at(ref,test,number):
    return precision(set(ref),set(test[0:number]))


def per_city_test(city_id):
    cur.execute('select site_id from sites_info where city_id = %s', (city_id, ))
    site_ids=cur.fetchall()[0:50]
    #print site_ids
    document_list=[]
    positive_ids=[]

    for id in site_ids:
        rating= random.randint(1,5)

        if rating >3:
            positive_ids.append(id[0])

        document={
            "documentId": id[0],
            "rating":rating
          }
        document_list.append(document)

    template["person"]["preferences"]=document_list
    template["location"]["id"]=city_id
    #data = {"location": {"lng": -85.6681, "state": "MI", "id": 192, "lat": 42.9634, "name": "Grand Rapids"}, "person": {"preferences": [{"documentId": 7, "rating": 4, "tags": ["Food & Drink"]}, {"documentId": 1, "rating": 1}], "id": 1}, "duration": "Longer", "season": "Spring", "group": "Friends"}
    data = json.dumps(template)
    #print template["person"]["preferences"]

    req = urllib2.Request(localhost, data, {'Content-Type': 'application/json'})
    #print data
    f = urllib2.urlopen(req)
    response =f.read()
    suggestions=json.loads(response)["suggestions"]

    return precision_at(positive_ids,suggestions,5)


def mean_city_test(city_numbers):
    cur.execute("select DISTINCT  city_id,city from sites_info order by city_id DESC LIMIT %s",(city_numbers,))
    result=cur.fetchall()
    #print result
    #city_list=[c[0] for c in result]
    #print city_list
    score=0
    n=0
    mean=0
    for city in result:
        n+=1
        city_id=city[0]
        city_name=city[1]
        print "city=",city_name
        s=per_city_test(city_id)
        score+=s
        print "P@10=",s," mean P@10=",score/n

    mean=score/n
    print "mean precision =",mean
    return mean

mean_city_test(100)
#per_city_test(151)