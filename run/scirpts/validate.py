import sqlite3, json, sys
import jsonschema
 
def validate(data, location = None, db_filename = 'docs2.db', schema_filename = 'response_schema.json'):
    with open(schema_filename) as f:
        schema = json.loads(f.read())
 
    jsonschema.validate(data, schema)
 
    conn = sqlite3.connect(db_filename)
    cur = conn.cursor()
 
    for id in data['suggestions']:
        cur.execute('select location from docs where id = ?', (id, ))
        row = cur.fetchone()
        if row is None:
            raise ValueError('ID is not a valid attraction', id)
        if int(location) and int(row[0]) != int(location):
            print location,"   ",row[0]
            raise ValueError('Attraction ID does not correspond to location', id, location)
 
    conn.close()
 
if __name__ == '__main__':
    if len(sys.argv) <= 1:
        print("usage: %s DATA [LOCATION]")
        exit()
    data_filename = sys.argv[1]
    with open(data_filename) as f:
        data = json.loads(f.read())
    location_id = None
    if len(sys.argv) == 3:
        location_id = sys.argv[2]
    validate(data, location_id)
    print('Valid')