__author__ = 'Jian Mo'

from elasticsearch import Elasticsearch
import json
import pystache
from nltk.stem.lancaster import LancasterStemmer
from nltk import RegexpTokenizer
import nltk
import operator


es = Elasticsearch()



text="The debugger caught an exception in your WSGI application. You can now look at the traceback which led to the error.To switch between the interactive traceback and the plaintext one, you can click on the  headline. From the text traceback you can also create a paste of it. For code execution mouse-over the frame you want to debug and click on the console icon on the right side.You can execute arbitrary Python code in the stack frames and there are some extra helpers available for introspection"

def mostCommon(text,number):
    toker = RegexpTokenizer(r'((?<=[^\w\s])\w(?=[^\w\s])|(\W))+', gaps=True)
    stemmer = LancasterStemmer()
    def stem_tokens(tokens, stemmer):
        stemmed = []
        for item in tokens:
            stemmed.append(stemmer.stem(item))
        return stemmed

    def tokenize(text):
        tokens = toker.tokenize(text)

        return tokens

    #new_text=stem_tokens(tokenize(text),stemmer)
    new_text=tokenize(text)
    stopwords = nltk.corpus.stopwords.words('english')
    allWordExceptStopDist = nltk.FreqDist(w.lower() for w in new_text if w not in stopwords)
    return " ".join([i[0] for i in allWordExceptStopDist.most_common(number)])



boosting_template1={
  "from": 0,
  "size": 100,
  "sort": {
    "_score": {
      "order": "desc"
    }
  },
  "query": {
    "bool": {
      "should": [
        {
          "boosting": {
            "positive": {
              "match": {
                "category": "mexican food"
              }
            },
            "negative": {
              "match": {
                "category": 151
              }
            },
            "negative_boost": 0.2
          }
        },
        {
          "boosting": {
            "positive": {
              "match": {
                "business_info": "japanese food"
              }
            },
            "negative": {
              "match": {
                "business_info": 151
              }
            },
            "negative_boost": 0.2
          }
        },
        {
          "boosting": {
            "positive": {
              "match": {
                "_all": "japanese food"
              }
            },
            "negative": {
              "match": {
                "_all": 151
              }
            },
            "negative_boost": 0.2
          }
        },
        {
          "boosting": {
            "positive": {
              "match": {
                "p_reviews": "p_reviews"
              }
            },
            "negative": {
              "match": {
                "n_reviews": "n_reviews"
              }
            },
            "negative_boost": 0.2
          }
        }
      ],
      "must": [
        {
          "match": {
            "city_id": 151
          }
        }
      ]
    }
  }
}
boosting_template2={
  "from": 0,
  "size": 100,
  "sort": {
    "_score": {
      "order": "desc"
    }
  },
  "query": {
    "bool": {
      "should": [
        {
          "boosting": {
            "positive": {
              "match": {
                "category": "mexican food"
              }
            },
            "negative": {
              "match": {
                "category": 151
              }
            },
            "negative_boost": 0.2
          }
        },
        {
          "boosting": {
            "positive": {
              "match": {
                "business_info": "japanese food"
              }
            },
            "negative": {
              "match": {
                "business_info": 151
              }
            },
            "negative_boost": 0.2
          }
        },
        {
          "boosting": {
            "positive": {
              "match": {
                "_all": "japanese food"
              }
            },
            "negative": {
              "match": {
                "_all": 151
              }
            },
            "negative_boost": 0.2
          }
        },
        {
          "boosting": {
            "positive": {
              "match": {
                "p_reviews": "p_reviews"
              }
            },
            "negative": {
              "match": {
                "n_reviews": "n_reviews"
              }
            },
            "negative_boost": 0.2
          }
        }
      ],
      "must": [
        {
          "match": {
            "city_id": 151
          }
        },
          {
          "match": {
            "site_id": "TRECCS-00147608-151"
          }
        }]
    }
  }
}

def elastic_rank(p_reviews="",n_reviews="",p_categories="",p_infos="",p_tags="",n_categories="",n_infos="",n_tags="",other_info="",city_id=151,site_id="",):
    #print mostCommon(p_reviews,50).encode("utf-8","ignore")
    if site_id=="":
        template=boosting_template1
        template["query"]["bool"]["must"][0]["match"]["city_id"]=city_id
    else:
        #print "calculating diversity score"
        template=boosting_template2
        template["query"]["bool"]["must"][1]["match"]["site_id"]=site_id

    template["query"]["bool"]["should"][0]["boosting"]["positive"]["match"]["category"]=p_categories
    template["query"]["bool"]["should"][1]["boosting"]["positive"]["match"]["business_info"]=p_infos
    template["query"]["bool"]["should"][2]["boosting"]["positive"]["match"]["_all"]=p_tags+other_info
    template["query"]["bool"]["should"][3]["boosting"]["positive"]["match"]["p_reviews"]=mostCommon(p_reviews,50)

    template["query"]["bool"]["should"][0]["boosting"]["negative"]["match"]["category"]=n_categories
    template["query"]["bool"]["should"][1]["boosting"]["negative"]["match"]["business_info"]=n_infos
    template["query"]["bool"]["should"][2]["boosting"]["negative"]["match"]["_all"]=n_tags
    template["query"]["bool"]["should"][3]["boosting"]["negative"]["match"]["n_reviews"]=mostCommon(n_reviews,50)

    # p_text=p_categories+p_infos+p_tags
    # n_text=n_categories+n_infos+n_tags
    # template["query"]["bool"]["should"][0]["boosting"]["positive"]["match"]["text"]=p_text
    # template["query"]["bool"]["should"][0]["boosting"]["negative"]["match"]["text"]=n_text
    # template["query"]["bool"]["must"][0]["match"]["city_id"]=city_id
    #print "template=",template
    ids=[]
    scores=[]
    with open("body_template","w+") as f:
        f.write(json.dumps(template))
        f.close()
    for hit in es.search("new_docs","docs",template)["hits"]["hits"]:
        id=hit["_source"]["site_id"]
        score=hit["_score"]
        ids.append(id)
        #print id,":",score
        scores.append(score)
    rank_dict=dict(zip(ids,scores))
   #rank_dict = sorted(rank_dict.items(), key=operator.itemgetter(1))

    #print rank_dict
    return rank_dict

#elastic_rank(p_categories)