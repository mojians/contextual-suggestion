import sqlite3
import numpy
from sklearn import svm
import operator
from difflib import get_close_matches
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.stem.porter import PorterStemmer
from sklearn.metrics.pairwise import linear_kernel

# open existing database
conn = sqlite3.connect('docs.db')
category_list=["Food","Music","Shopping","Night Life","Sports"]
category_no=len(category_list)

c = conn.cursor()
#categorylist=
# print all lines ordered by integer value in my_var2
# for row in c.execute('SELECT * FROM docs  '):
#     array=numpy.array(row)
#     print array[3]
#     print row

X=numpy.array([[4., 0., 0., 0., 1.],
               [4., 1., 0., 0., 0.],
               [3., 0.,1., 0., 0.],
               [2., 0.,0., 1., 0.],
               [2., 0.,0., 1., 0.],])
Y=numpy.array([4,3,2,1,0])
#print X , Y
def category_process(input):
    return input


def build_training_array(site_ids,user_tags):
    conn = sqlite3.connect('docs.db')
    c = conn.cursor()
    site_ratings=[]
    categories=[]
    #review_count=
    #
    for site_id in site_ids:
        print "site id",site_id
        site_rating=c.execute("SELECT rating FROM docs WHERE id=?",(site_id,))
        #category=c.execute("SELECT category FROM docs WHERE id=?",(site_id,))
        site_rating=float(site_rating.fetchone()[0])
        temp_ls=[]
        temp_ls.append(site_rating)
        print "site",site_rating
        site_ratings.append(temp_ls)
        #categories.append(category)
    print site_ratings,"site rating"
    X=numpy.array(site_ratings)
    print X
    return X

def train(X,Y):
    Y=numpy.array(Y)
    print "X",X
    print "Y",Y
    clf = svm.SVC(gamma=0.001, C=100.)
    clf.fit(X, Y)
    #print clf.predict([3,0,1,0,0])
    return clf


def rank(clf,location):
    i=0
    conn = sqlite3.connect('docs.db')
    c = conn.cursor()
    c.execute('select id from docs where location = ?', (location, ))
    ids = c.fetchall()
    c.execute('select rating from docs where location = ?', (location, ))
    site_rating=c.fetchall()
    result={}
    data_ls=unwrap(site_rating)
    ids=unwrap(ids,False)
    data_dict = dict(zip(ids,data_ls ))
    print data_dict

    result_dict={}
    for key in data_dict.keys():
        print clf.predict(data_dict[key])
        result_dict[key]=clf.predict(data_dict[key])[0]
    #result_dict={"2":5,"3":1}
    sorted_dict = sorted(result_dict.items(), key=operator.itemgetter(1))
    return sorted_dict


def learn_rank(site_ids,user_tags,user_ratings,location):
    X=build_training_array(site_ids,user_tags)
    Y=numpy.array(user_ratings)
    clf=train(X,Y)
    print rank(clf,location)




def unwrap(ls,wrap=True):
    data_ls=[]
    for t in ls:
        #print "t=",t[0]
        temp_ls=[]
        try:
            item=float(t[0])
        except:
            item=0.0
        if wrap==True:
            temp_ls.append(item)
            data_ls.append(temp_ls)
        else:
            item=str(int(item))
            data_ls.append(item)
    return data_ls


stemmer = PorterStemmer()


def stem_tokens(tokens, stemmer):
    stemmed = []
    for item in tokens:
        stemmed.append(stemmer.stem(item))
    return stemmed

def tokenize(text):
    tokens = nltk.word_tokenize(text)
    stems = stem_tokens(tokens, stemmer)
    return stems

def tag_similarity(tags,text):
    count=0
    if type(tags) is str:
        tags=tags.split()
    if type(text) is str:
        text=text.split()
    for tag in tags:
        count=count+len(get_close_matches(tag, text))
        #print get_close_matches(tag, text)
       # print count
    return  count

# tag_similarity("apple banana","apple banana oranges pineapple")

learn_rank(["0","2"],"",[5,4],"296")