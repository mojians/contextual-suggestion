# -*- coding: utf-8 -*-

# Scrapy settings for yelp project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#
ELASTICSEARCH_SERVER = 'localhost'
ELASTICSEARCH_PORT = 9200
ELASTICSEARCH_INDEX = 'test_docs'
ELASTICSEARCH_TYPE = 'docs'
ELASTICSEARCH_UNIQ_KEY = 'url'


BOT_NAME = 'yelp_profile'

SPIDER_MODULES = ['yelp_profile.spiders']
NEWSPIDER_MODULE = 'yelp_profile.spiders'
#USER_AGENT = "Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19"
ITEM_PIPELINES = ['yelp_profile.pipelines.YelpProfilePipeline','scrapyelasticsearch.scrapyelasticsearch.ElasticSearchPipeline']
DEPTH_LIMIT=0
DEPTH_PRIORITY=1





import os

basedir = os.path.abspath(os.path.dirname(__file__))

#SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'YelpData.db')
SQLALCHEMY_DATABASE_URI = 'mysql://root:3231862@localhost:3306/test_docs?charset=utf8'
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')


# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'yelp (+http://www.yourdomain.com)'
