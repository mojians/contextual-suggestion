__author__ = 'Jian Mo'
from pony import orm
from pony import *
import _socket
import MySQLdb
from gevent.pool import  Pool
import gevent.monkey
gevent.monkey.patch_all()
from sqlalchemy.orm import sessionmaker

DB = dict(host="localhost",
          user="root",
          passwd="3231862",
          db="new_docs",
          )

db = orm.Database("mysql", **DB)

class Docs(db.Entity):
    _table_="docs"
    unique_id=orm.PrimaryKey(int)
    id=orm.Required(str)
    city_id=orm.Optional(str)
    website=orm.Optional(str)
    title=orm.Optional(str)
    city=orm.Optional(str)
    yelp=orm.Optional(str)

db.generate_mapping(check_tables=True, create_tables=False)

with orm.db_session:
    docs=orm.select(p for p in Docs)
    i=0
    for doc in docs:
        doc.unique_id=i
        i=i+1
