# -*- coding: utf-8 -*-



import scrapy


class Sites_item(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    site_id =scrapy.Field()
    name =scrapy.Field()
    rating =scrapy.Field()
    snippet =scrapy.Field()
    category =scrapy.Field()
    url =scrapy.Field()
    address =scrapy.Field()
    reviews =scrapy.Field()
    review_count =scrapy.Field()
    positive_reviews=scrapy.Field()
    negative_reviews=scrapy.Field()
    price=scrapy.Field()
    hours=scrapy.Field()
    business_info=scrapy.Field()
    city_id=scrapy.Field()
    city=scrapy.Field()
    website=scrapy.Field()

class Reviews_item(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    review_id=scrapy.Field()
    review_text = scrapy.Field()
    review_rating = scrapy.Field()
    reviewer = scrapy.Field()
    url = scrapy.Field()
    site_id = scrapy.Field()
    site_name = scrapy.Field()
    site_rating = scrapy.Field()
    site_category=scrapy.Field()