# coding= utf-8

__author__ = 'Jian Mo'
import umysql
import umysqldb
umysqldb.install_as_MySQLdb()
import mechanize
from urllib import urlencode
from scrapy.selector import Selector
import _socket
import MySQLdb
# import sqlite3
from gevent.pool import  Pool
import gevent.monkey
gevent.monkey.patch_all()
import httplib2
gevent.monkey.patch_socket()


yelp_url = "http://www.yelp.ca/"
i=0
user_agent="Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36"
url_dict={}
data_list=[]


def ungzipResponse(r,b):
	headers = r.info()
	if headers['Content-Encoding']=='gzip':
		import gzip
		gz = gzip.GzipFile(fileobj=r, mode='rb')
		html = gz.read()
		gz.close()
		headers["Content-type"] = "text/html; charset=utf-8"
		r.set_data( html )
		b.set_response(r)


def search(desc, location):
    # br = mechanize.Browser()
    # br.set_handle_robots(False)
    # br.addheaders = [('User-agent', user_agent)]
    #
    # br.addheaders.append( ['Accept-Encoding','gzip'] )
    # desc=desc.replace(u"\u2018", "'").replace(u"\u2019", "'")
    #
    # try: #search yelp
    #     base_url = "http://m.yelp.com/search?"
    #     #base_url="http://stackoverflow.com/questions/15322701/gevent-pool-with-nested-web-requests"
    #     mapsize = "902 567"
    #     parameter = {"find_desc": desc, "find_loc": location, "mapsize": mapsize}
    #     paras = urlencode(parameter)
    #     query = base_url + paras
    #     query.replace("++","+")
    #
    #     print "yelp_query=",query
    #     # query="http://m.yelp.com/"+"search?find_desc="+desc%20park&find_loc=Qu%C3%A9bec%2C%20QC&mapsize=1085%2C499
    #     r = br.open(query)
    #     ungzipResponse(r,br)
    #     html=r.read()
    #     with open("page.html","w+") as f:
    #         f.write(html)
    #         f.close()
    #
    #     sel = Selector(text=html)
    #     # site_url = sel.xpath(
    #     #     '//span[@class="indexed-biz-name"]/a[@class="biz-name"]/@href').extract()[0]  #normal yelp extract
    #     # print "yelp query=",query
    #     site_url=sel.xpath('//[@id="wrap"]/div[2]/div[4]/div[2]/ol/li[1]/div[2]/h3/a/@href').extract()[1]
    #except: #search google
        # print "search bing instead"
        br = mechanize.Browser()
        br.set_handle_robots(False)
        br.addheaders = [('User-agent', user_agent)]

        br.addheaders.append( ['Accept-Encoding','gzip'] )

        base_url = "http://m.search.aol.com/search?s_it=searchbox.webhome&v_t=na&"
        parameter = {"q": desc+" "+location+" www.yelp.com"}
        paras = urlencode(parameter)

        query = base_url + paras
        query.replace("++","+")
        print "query=",query
        #br.addheaders = [('User-agent', "chrome")]

        # h = httplib2.Http()
        # resp, content = h.request(query)
        r = br.open(query)
        ungzipResponse(r,br)
        html=r.read()
        #html=content

        # with open("bing_page.html","w+") as f:
        #     f.write(html)
        #     f.close()



        sel = Selector(text=html)
        # try:
        #print html
        # site_url=sel.xpath('//*[@id="b_results"]/li[2]/h2/a/@href').extract()[0]  #for bing
        try:
            site_url=sel.xpath('//*[@id="c"]/div/div[1]/ul/li/h3/a/@href').extract()[0]
        except:
            return None
        # if "yelp" not in site_url:
        #     site_url=sel.xpath('//h2[@style="position: relative;"]/a/@href').extract()[1] #for bing


        # except:
        #     site_url=sel.xpath('//h2[@style="position: relative;"]/a/@href').extract()[1] #for bing
        #     pass
        #print "length=",len(data_list)
        if "yelp" not in site_url:
            return None
        site_url=site_url.replace("http://www.yelp.ca","")
        site_url=site_url.replace("http://m.yelp.ca","")
        site_url=site_url.replace("http://m.yelp.com","")
        site_url=site_url.replace("http://www.yelp.com","")
        print "site_url=",site_url
        return site_url


def write2database():
    rows=[]
    pool=Pool(1)
#     # print id,type(id)
    cur.execute(
    'select  id,title,yelp,city from docs ')
    rows=cur.fetchall()
    cur.close()
#     #print len(rows)
    for row in rows:
        inc()
        pool.spawn(get_dict,row)
    #     if len(data_list)%100==0 and len(data_list)<>0:
    # #         #print "data_list=",data_list
    #         commit_data(data_list)
    #         print "data committed"
    #
    pool.join()


def get_dict(row):

    data_dic={}
    data_dic["id"]=row[0]
    #row=dict(row)
    #
    title = row[1]
    yelp=row[2]
    city=row[-1]
#     #print city
    if yelp:
        pass
    else:
        print row
        search_result=search(title, city)
        # print "search_result=",search_result
        if search_result:
            yelp = yelp_url + search_result
        else:
            yelp="no result"
        # print "yelp=",yelp
        data_dic["yelp"]=yelp
        data_list.append(data_dic)
        #sql_update(data_dic)
#     #print "data_list=",data_list
#     #print "length(list)=",len(data_list)

#         #print "deleted"
    return data_dic

def inc():
    global i
    i=i+1
    if len(data_list)>=1 and  len(data_list)<>0 :
        commit_data(data_list)
        del data_list[:]
    print i/250000.0


def commit_data(list):
    # conn.row_factory = sqlite3.Row
    # #sql="UPDATE docs SET yelp='%s' WHERE id='%s'"
    sql_pool=Pool(1)
    for item in list:
        sql_pool.spawn(sql_update,item)
    sql_pool.join()
    print "data committed"

def sql_update(item):
    print "item=",item
        # print type(item["yelp"]),type(item["id"])
        # sql=sql%(item["yelp"],str(item["id"]))
#         #print "item=",item

    #cur.execute('SELECT yelp FROM docs FOR UPDATE')
    con = umysql.Connection()
    con.connect('localhost', 3306, 'root', '3231862', 'test_docs', 1, 'utf8')
    try:
        con.query('UPDATE docs SET yelp=%s WHERE id=%s', (item["yelp"], item["id"]))
    except:
        con.query('UPDATE docs SET yelp=%s WHERE id=%s', ("no result",item["id"]))
    print "new url updated"
    con.close()

def main():
    write2database()


if __name__ == '__main__':
    conn = MySQLdb.connect(host="localhost", # your host, usually localhost
                     user="root",passwd="3231862",
                      db="test_docs") # name of the data base
    #conn = sqlite3.connect('test_docs.db')
    cur = conn.cursor()
    main()
    conn.commit()
    conn.close()