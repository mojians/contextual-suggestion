# -*- coding: utf-8 -*-

from pony import orm
from pony import *
import mechanize
from urllib import urlencode
from scrapy.selector import Selector
import _socket
from gevent.pool import  Pool
import gevent.monkey
gevent.monkey.patch_all()
from sqlalchemy.orm import sessionmaker
import MySQLdb

DB = dict(host="localhost",
          user="root",
          passwd="3231862",
          db="new_docs",
          )

db = orm.Database("mysql", **DB)


yelp_url = "http://www.yelp.ca/"
i=0
user_agent="Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36"
url_dict={}
data_list=[]


class Docs(db.Entity):
    _table_="docs"
    uid=orm.PrimaryKey(int)
    id=orm.Optional(str)
    city_id=orm.Optional(str)
    website=orm.Optional(str)
    title=orm.Optional(str)
    city=orm.Optional(str)
    yelp=orm.Optional(str)

db.generate_mapping(check_tables=True, create_tables=False)

def ungzipResponse(r,b):
	headers = r.info()
	if headers['Content-Encoding']=='gzip':
		import gzip
		gz = gzip.GzipFile(fileobj=r, mode='rb')
		html = gz.read()
		gz.close()
		headers["Content-type"] = "text/html; charset=utf-8"
		r.set_data( html )
		b.set_response(r)


def search(desc, location):
    # br = mechanize.Browser()
    # br.set_handle_robots(False)
    # br.addheaders = [('User-agent', user_agent)]
    #
    # br.addheaders.append( ['Accept-Encoding','gzip'] )
    # desc=desc.replace(u"\u2018", "'").replace(u"\u2019", "'")
    #
    # try: #search yelp
    #     base_url = "http://m.yelp.com/search?"
    #     #base_url="http://stackoverflow.com/questions/15322701/gevent-pool-with-nested-web-requests"
    #     mapsize = "902 567"
    #     parameter = {"find_desc": desc, "find_loc": location, "mapsize": mapsize}
    #     paras = urlencode(parameter)
    #     query = base_url + paras
    #     query.replace("++","+")
    #
    #     print "yelp_query=",query
    #     # query="http://m.yelp.com/"+"search?find_desc="+desc%20park&find_loc=Qu%C3%A9bec%2C%20QC&mapsize=1085%2C499
    #     r = br.open(query)
    #     ungzipResponse(r,br)
    #     html=r.read()
    #     with open("page.html","w+") as f:
    #         f.write(html)
    #         f.close()
    #
    #     sel = Selector(text=html)
    #     # site_url = sel.xpath(
    #     #     '//span[@class="indexed-biz-name"]/a[@class="biz-name"]/@href').extract()[0]  #normal yelp extract
    #     # print "yelp query=",query
    #     site_url=sel.xpath('//[@id="wrap"]/div[2]/div[4]/div[2]/ol/li[1]/div[2]/h3/a/@href').extract()[1]
    #except: #search google
        # print "search bing instead"
        br = mechanize.Browser()
        br.set_handle_robots(False)
        br.addheaders = [('User-agent', user_agent)]

        br.addheaders.append( ['Accept-Encoding','gzip'] )

        base_url = "http://m.search.aol.com/search?s_it=searchbox.webhome&v_t=na&"
        parameter = {"q": desc+" "+location+" www.yelp.com"}
        paras = urlencode(parameter)

        query = base_url + paras
        query.replace("++","+")
        print "query=",query
        #br.addheaders = [('User-agent', "chrome")]

        # h = httplib2.Http()
        # resp, content = h.request(query)
        r = br.open(query)
        ungzipResponse(r,br)
        html=r.read()
        #html=content

        # with open("bing_page.html","w+") as f:
        #     f.write(html)
        #     f.close()



        sel = Selector(text=html)
        # try:
        #print html
        # site_url=sel.xpath('//*[@id="b_results"]/li[2]/h2/a/@href').extract()[0]  #for bing
        site_url=sel.xpath('//*[@id="c"]/div/div[1]/ul/li/h3/a/@href').extract()[0]
        # if "yelp" not in site_url:
        #     site_url=sel.xpath('//h2[@style="position: relative;"]/a/@href').extract()[1] #for bing

        print "site_url=",site_url
        # except:
        #     site_url=sel.xpath('//h2[@style="position: relative;"]/a/@href').extract()[1] #for bing
        #     pass
        #print "length=",len(data_list)
        if "yelp" not in site_url:
            return None
        site_url=site_url.replace("http://www.yelp.ca","")
        site_url=site_url.replace("http://m.yelp.ca","")
        site_url=site_url.replace("http://m.yelp.com","")
        site_url=site_url.replace("http://www.yelp.com","")

        return site_url

@orm.db_session
def write2database():
    pool=Pool(1)
    cur.execute(
    'select  id,title,yelp,city from docs ')
    rows=cur.fetchall()
    cur.close()
    docs=orm.select(d for d in Docs)
    for doc in docs:
        get_dict(doc)


def get_dict(doc):
    with orm.db_session:
        if doc.yelp is None:
            print doc
            search_result=search(doc.title, doc.city)
            if search_result <> None:
                yelp = yelp_url + search_result
            else:
                yelp="no result"
                #data_list.append(data_dic)
                doc.yelp=yelp
            print "data updated"
            db.commit()
    return None

def inc():
    global i
    i=i+1
    if i%100==0:
        db.commit()

def main():
    pool=Pool(100)
    pool.spawn(write2database())
    pool.join()
if __name__ == '__main__':
    conn = MySQLdb.connect(host="localhost", # your host, usually localhost
                     user="root",passwd="3231862",
                      db="new_docs") # name of the data base
    #conn = sqlite3.connect('new_docs.db')
    cur = conn.cursor()
    main()
