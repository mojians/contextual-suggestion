# -*- coding: utf-8 -*-


from gevent import monkey; monkey.patch_all()
from pony import *
import gevent

db=Database()
DB = dict(host="localhost",
          user="root", 
          passwd="3231862",
          db="gevent_test",
          )

db = orm.Database("mysql", **DB)


class Docs(db.Entity):
    _table_="docs"
    id=orm.Required(str)
    city_id=orm.Optional(str)
    website=orm.Optional(str)
    title=orm.Optional(str)
    city=orm.Optional(str)
    yelp=orm.Optional(str)


class Person(db.Entity):
    _table_ = "person"
    
    name = orm.Required(unicode)
    age = orm.Required(int)
    motto = orm.Optional(unicode)
    cars = orm.Set("Car")


class Car(db.Entity):
    _table_ = "car"
    
    owner = orm.Required(Person)
    model = orm.Required(unicode) 
    

# orm.sql_debug(True)
db.generate_mapping(create_tables=True)

def test_insert1():
    print "test_insert1 start.."
    with orm.db_session:
        p1 = Person(name="fk", age=24)
        car1 = Car(owner=p1, model="Lexus")
    print "test_insert1 end.."

def test_insert2():
    print "test_insert2 start.."
    with orm.db_session:
        p1 = Person(name="gp", age=24)
        car1 = Car(owner=p1, model="Lexus")
    print "test_insert2 end.."

gevent.spawn(test_insert1)
gevent.spawn(test_insert2)
print "main coroutine.."

gevent.wait()


__all__ = ["Person", "Car", "db"]