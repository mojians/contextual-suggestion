from sqlalchemy import create_engine, Column, Integer, String, DateTime, ForeignKey,Float,Unicode,TEXT,VARCHAR
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.engine.url import URL
from sqlalchemy.orm import relationship
from sqlalchemy import UniqueConstraint
import settings


DeclarativeBase = declarative_base()


def db_connect():
    """
    Performs database connection using database settings from settings.py.
    Returns sqlalchemy engine instance
    """
    return create_engine(settings.SQLALCHEMY_DATABASE_URI)


def create_deals_table(engine):
    """"""
    DeclarativeBase.metadata.create_all(engine)


class Sites(DeclarativeBase):

    """Sqlalchemy sites model"""
    __tablename__ = "sites_info"
    id = Column(Integer, primary_key=True)
    site_id = Column("site_id", VARCHAR(25),unique=True)
    name = Column('name', TEXT(500))
    rating = Column("rating",Float)
    snippet = Column('snippet', TEXT(100))
    category = Column('category', TEXT(100))
    url = Column('url', TEXT(100))
    address = Column('address', VARCHAR(150))
    #reviews = relationship('Reviews', backref='place', lazy="dynamic")
    review_count = Column('review_count', Integer)
    positive_reviews=Column('p_reviews', TEXT(100000))
    negative_reviews=Column('n_reviews', TEXT(100000))
    price=Column("price", TEXT(20))
    hours=Column("hours", VARCHAR(150))
    business_info=Column("business_info", TEXT(500))
    city_id=Column("city_id", Integer)
    city=Column("city",VARCHAR(50))
    website=Column("website",VARCHAR(100))

class Reviews(DeclarativeBase):

    """Sqlalchemy reviews model"""
    __tablename__ = "reviews"
    id = Column(Integer, primary_key=True)
    review_id=Column("review_id",TEXT(300))
    review_text = Column('review_text', TEXT(500000))
    review_rating = Column('review_rating', Float)
    reviewer=Column('reviewer', TEXT(300))
    url=Column('url', TEXT(100))
    #site_id = Column("site_id",Integer, ForeignKey("sites.site_id"))
    site_name=Column('site_name', TEXT(100) )
    site_rating=Column("site_rating",Float)
    site_category = Column('site_category', TEXT(500))


