# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from sqlalchemy.orm import sessionmaker
from models import Sites, db_connect, create_deals_table,Reviews
from items import Sites_item,Reviews_item
from pony import orm

class YelpProfilePipeline(object):
    """Livingsocial pipeline for storing scraped items in the database"""
    def __init__(self):
        """
        Initializes database connection and sessionmaker.
        Creates deals table.
        """
        engine = db_connect()
        create_deals_table(engine)
        self.Session = sessionmaker(bind=engine)

    def process_item(self, item, spider):
        """Save deals in the database.

        This method is called for every item pipeline component.

        """
        session = self.Session()

        if isinstance(item,Sites_item):
            Dataitem = Sites(**item)
            print item
        if isinstance(item,Reviews_item):
            #Dataitem = Reviews(**item)
            pass

        try:
            session.add(Dataitem)
            session.commit()
        except:
            session.rollback()
            raise
        finally:
            session.close()

        return item


class PonyPipelines(object):
    pass



