__author__ = 'Jian Mo'
# -*- coding: utf-8 -*-

from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.lxmlhtml import LxmlLinkExtractor
from yelp_profile.items import Sites_item, Reviews_item
#import simplejson as json
from scrapy.http import Request
import MySQLdb
import json
import umysql

baseUrl = "www.yelp.ca"

class MySpider(CrawlSpider):
    name = "yelp_profile"
    allowed_domains = ["yelp.ca"]
    start_urls=[]



    def start_requests(self):
        conn = MySQLdb.connect(user='root',passwd='3231862',db='test_docs',
                host='localhost',
                charset="utf8",
                use_unicode=True
                )
        cursor = conn.cursor()
        cursor.execute('SELECT yelp,id,city_id,city,website FROM docs;')
        rows = cursor.fetchall()

        for row in rows:
            sid1=row[1]
            #print sid1
            sid2=cursor.execute('SELECT site_id FROM sites_info WHERE site_id = %s',(sid1,))
            print sid1," ",sid2
            if row[0] and (sid2 ==0):
                #print row
                if "biz" and "http" in row[0]:
                    yield Request(row[0],callback=self.parse_info, meta={'site_id': row[1],'city_id':row[2],'city':row[3],'website':row[4]})
    #start_urls=["http://www.yelp.ca/biz/novel-restaurant-kansas-city"]
    # # url_dict=json.loads(f,'latin-1')
    # # print url_dict
    # # start_urls = [url_dict[l] for l in url_dict]
    #
    #
    # #start_urls = [l.strip() for l in open('H:\WareHouse\TREC\Contextual Suggestion\crawler\yelp_profile\yelp_profile\urls.txt').readlines()]
    # #start_urls = [
    #     #"http://www.yelp.ca/search?find_desc=Millennium+Park&find_loc=chicago&mapsize=1087%2C-542&start=0"]
    #start_urls = ["http://www.yelp.ca/biz/millennium-park-chicago"]
    # rules = (
    #     Rule(LxmlLinkExtractor(allow=('(?=.*biz).*'),
    #                            restrict_xpaths=('//a[@class="biz-name"]')), callback="parse_reviews", follow=True),
    #     Rule(LxmlLinkExtractor(allow=('(?=.*biz)(?=.*start\=).*'), restrict_xpaths=('//a[@class="page-option available-number"]')),
    #          callback="parse_reviews", follow=True),
    #     Rule(LxmlLinkExtractor(allow=("(?=.*search\?find)(?=.*start\=).*"), restrict_xpaths=('//a[@class="page-option available-number"]')),
    #          callback="parse_sites", follow=True)
    # )

    def parse_start_url(self, response):
        print "parsing start"
        return self.parse_info(response)

    def parse_sites(self, response):
        print "called"
        sel = HtmlXPathSelector(response)
        sites = sel.select(
            '//div[@class="search-result natural-search-result"]')
        item = Sites_item()
        for site in sites:

            item["site_id"] = " ".join(
                site.xpath('.//a[@class="biz-name"]/@data-hovercard-id').extract())
            item["name"] = " ".join(
                site.xpath('.//a[@class="biz-name"]//text()').extract())
            item["rating"] = float(
                " ".join(site.xpath('.//div[@class="rating-large"]/i/@title').extract())[0][:2])
            item["snippet"] = unicode(
                "".join(site.xpath('.//p[@class="snippet"]/text()').extract()))
            item["category"] = " ".join(
                site.xpath('.//span[@class="category-str-list"]//text()').extract()).lstrip().rstrip()
            item["url"] = baseUrl + \
                " ".join(site.xpath('.//a[@class="biz-name"]/@href').extract())
            item["address"] = " ".join(
                site.xpath('.//address//text()').extract())
            item["review_count"] = int(site.xpath(
                './/span[@class="review-count rating-qualifier"]/text()').extract()[0].lstrip().partition(" ")[0])

            yield item

    def parse_reviews(self, response):
        print "parsing reviews"
        sel = HtmlXPathSelector(response)
        reviews = sel.xpath('//div[@class="review review--with-sidebar"]')
        item = Reviews_item()
        print reviews
        for review in reviews:
            item['review_id'] = " ".join(
                review.xpath('.//@data-review-id').extract())
            item['reviewer'] = " ".join(
                review.xpath('.//li[@class="user-name"]/a/text()').extract())
            item['review_rating'] = float(
                " ".join(review.xpath('.//meta[@itemprop="ratingValue"]/@content').extract()))
            item['review_text'] = " ".join(
                review.xpath('.//p[@itemprop="description"]/text()').extract())

        item['site_name'] = " ".join(
            sel.xpath('//a[@class="biz-name"]/text()').extract())
        item['site_id'] = " ".join(
            sel.xpath('//a[@class="biz-name"]/@data-hovercard-id').extract())
        item['site_rating'] = float(
            sel.xpath('//meta[@itemprop="ratingValue"]/@content').extract()[0])
        item["site_category"] = " ".join(
            sel.xpath('.//span[@class="category-str-list"]//text()').extract()).lstrip().rstrip()
        item['url'] = response.url
            # print item['review_id']
        yield item

    def parse_info(self, response):
        print "parsing info"
        sel = HtmlXPathSelector(response)
        item=Sites_item()
        positive_reviews=""
        negative_reviews=""

        reviews = sel.xpath('//div[@class="review review--with-sidebar"]')
        for review in reviews:
            rating = float(
                " ".join(review.xpath('.//meta[@itemprop="ratingValue"]/@content').extract()))
            comment = " ".join(
                review.xpath('.//p[@itemprop="description"]/text()').extract())
            if rating>3.0:
                positive_reviews+=comment
            if rating<3.0:
                negative_reviews+=comment
        try:
            info_keys=sel.xpath('//div[@class="short-def-list"]//dt[@class="attribute-key"]/text()').extract()
            info_values=sel.xpath('//div[@class="short-def-list"]//dl/dd/text()').extract()
            info_keys=map(lambda x: x.strip(), info_keys)
            info_values=map(lambda x: x.strip(), info_values)

            business_info=json.dumps(dict(zip(info_keys, info_values)))
            #print "business_info=",business_info
        except:
            business_info=""
        #print "type(business_info)=",type(business_info)

        hours_keys=sel.xpath('//table[@class="table table-simple hours-table"]/tbody/tr/th[@scope="row"]/text()').extract()
        hours_values=sel.xpath('//table[@class="table table-simple hours-table"]/tbody/tr//td[1]//text()').extract()
        hours_keys=map(lambda x: x.strip(), hours_keys)
        # this block to parse hours values list
        hours_values=map(lambda x: x.strip(), hours_values)
        #print hours_values
        l=[]
        for i in range(0,len(hours_values)-1):
            if hours_values[i+1]=="" and hours_values[i]<>"" and hours_values[i-1]<>"-":
                l.append(hours_values[i])
            if hours_values[i+1]=="-":
                l.append(hours_values[i]+hours_values[i+1]+hours_values[i+2])
        hours_values=l
        hours=json.dumps(dict(zip(hours_keys, hours_values)))

        price=" ".join(
            sel.xpath('//*[@id="super-container"]/div/div/div[2]/div[1]/div[2]/ul/li[2]/div[2]/dl/dd/text()').extract()).strip()
        name = " ".join(
            sel.xpath('//a[@class="biz-name"]/text()').extract())
        #site_id = " ".join(
            #sel.xpath('//a[@class="biz-name"]/@data-hovercard-id').extract())
        rating =float(sel.xpath('//div[@class="biz-rating biz-rating-very-large clearfix"]/div/meta[@itemprop="ratingValue"]/@content').extract()[0])
        print rating

        category = " ".join(
            [i.strip() for i in sel.xpath('//span[@class="category-str-list"]//text()').extract()])
        address = " ".join(
            sel.xpath('//address[@itemprop="address"]//text()').extract()).strip()
        review_count = int(sel.xpath(
                '//div[@class="biz-page-header-left"]//span/span[@itemprop="reviewCount"]/text()').extract()[0].lstrip().partition(" ")[0])
        url = response.url



        item['business_info']=business_info if business_info else ""
        item['hours']=hours if hours else ""
        item['positive_reviews']=positive_reviews if positive_reviews else ""
        item['negative_reviews']=negative_reviews if negative_reviews else ""
        item["price"] =price if price else ""
        item['name'] =name if name else ""

        item['rating'] =rating if rating else ""
        item["category"] =category if category else ""
        item["address"] =address if address else ""
        item["review_count"] =review_count if review_count else ""
        item['url'] =url if url else ""

        item['site_id'] =response.meta['site_id']
        item['city_id'] =int(response.meta['city_id'])
        item['city'] =response.meta['city']
        item['website'] =response.meta['website']
        yield item
