__author__ = 'Jian Mo'
import csv
from bs4 import BeautifulSoup as bs
import mechanize
from urllib import urlencode
from scrapy.selector import Selector
import json
import sqlite3
yelp_url = "http://www.yelp.ca/"
conn = sqlite3.connect('H:/WareHouse/TREC/Contextual Suggestion/crawler/yelp_profile/yelp_profile/YelpData.db')


def search(desc, location):
    br = mechanize.Browser()
    br.set_handle_robots(False)
    br.addheaders = [('User-agent', 'chrome')]
    base_url = "http://www.yelp.ca/search?"
    location = "chicago"
    mapsize = "902 567"
    parameter = {"find_desc": desc, "find_loc": location, "mapsize": mapsize}
    paras = urlencode(parameter)
    query = base_url + paras
    # query="http://m.yelp.com/"+"search?find_desc="+desc%20park&find_loc=Qu%C3%A9bec%2C%20QC&mapsize=1085%2C499
    html = br.open(query).read()
    sel = Selector(text=html)
    site_url = sel.xpath(
        '//span[@class="indexed-biz-name"]/a[@class="biz-name"]/@href').extract()[0]
    return site_url


def main():
    dict = {}

    with open('contexts2015.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            title = row['title']
            city=row['city']
            print title
            try:
                url = yelp_url + search(title, city)
                dict[title] = url
                csvfilewrite(url)
                print url
            except:
                print "no result"
    print type(dict)
    with open("profile_urls.txt", "w") as outfile:
        json.dump(dict,outfile,ensure_ascii=False)


#main()