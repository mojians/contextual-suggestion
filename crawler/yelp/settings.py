# -*- coding: utf-8 -*-

# Scrapy settings for yelp project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'yelp'

SPIDER_MODULES = ['yelp.spiders']
NEWSPIDER_MODULE = 'yelp.spiders'
#USER_AGENT = "Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19"
ITEM_PIPELINES = ['yelp.pipelines.YelpPipeline']



import os

basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'YelpData.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')


# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'yelp (+http://www.yourdomain.com)'
