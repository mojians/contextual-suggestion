from sqlalchemy import create_engine, Column, Integer, String, DateTime, ForeignKey,Float,Unicode
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.engine.url import URL
from sqlalchemy.orm import relationship
from sqlalchemy import UniqueConstraint
import settings


DeclarativeBase = declarative_base()


def db_connect():
    """
    Performs database connection using database settings from settings.py.
    Returns sqlalchemy engine instance
    """
    return create_engine(settings.SQLALCHEMY_DATABASE_URI)


def create_deals_table(engine):
    """"""
    DeclarativeBase.metadata.create_all(engine)


class Sites(DeclarativeBase):

    """Sqlalchemy sites model"""
    __tablename__ = "sites"
    id = Column(Integer, primary_key=True)
    site_id = Column("site_id", String,unique=True)
    name = Column('name', String,unique=True)
    rating = Column("rating",Float)
    snippet = Column('snippet', Unicode)
    category = Column('category', String)
    url = Column('url', String,unique=True)
    address = Column('address', String,unique=True)
    reviews = relationship('Reviews', backref='place', lazy="dynamic")
    review_count = Column('review_count', Integer,unique=True)

class Reviews(DeclarativeBase):

    """Sqlalchemy reviews model"""
    __tablename__ = "reviews"
    id = Column(Integer, primary_key=True)
    review_id=Column("review_id",String, unique=True)
    review_text = Column('review_text', String)
    review_rating = Column('review_rating', Float)
    reviewer=Column('reviewer', String)
    url=Column('url', String)
    site_id = Column("site_id",Integer, ForeignKey("sites.site_id"))
    site_name=Column('site_name', String )
    site_rating=Column("site_rating",Float)