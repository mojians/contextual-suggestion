__author__ = 'Jian Mo'
# -*- coding: utf-8 -*-

from scrapy.selector 		import HtmlXPathSelector
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.lxmlhtml import LxmlLinkExtractor
from yelp.items		import Sites_item, Reviews_item
from scrapy.http		import Request
baseUrl="www.yelp.ca"

class MySpider(CrawlSpider):
    name 		= "yelp"
    allowed_domains	= ["yelp.ca"]
    start_urls	= ["http://www.yelp.ca/search?find_desc=Millennium+Park&find_loc=chicago&mapsize=1087%2C-542&start=0"]
    #start_urls	= ["http://www.yelp.ca/biz/millennium-park-chicago"]
    rules = (
    Rule(LxmlLinkExtractor(allow=('(?=.*biz).*'),restrict_xpaths=('//a[@class="biz-name"]')),callback="parse_reviews",follow=True),
    Rule(LxmlLinkExtractor(allow=('(?=.*biz)(?=.*start\=).*' ),restrict_xpaths=('//a[@class="page-option available-number"]')),
         callback="parse_reviews",follow=True),
    Rule(LxmlLinkExtractor(allow=("(?=.*search\?find)(?=.*start\=).*"),restrict_xpaths=('//a[@class="page-option available-number"]')),
         callback="parse_sites",follow=True)
    )

    def parse_start_url(self, response):
        self.parse_sites(response)

    def parse_sites(self,response):
        print "called"
        sel = HtmlXPathSelector(response)
        sites=sel.select('//div[@class="search-result natural-search-result"]')
        item = Sites_item()
        for site in sites:

            item["site_id"] = " ".join(site.xpath('.//a[@class="biz-name"]/@data-hovercard-id').extract())
            item["name"] =" ".join(site.xpath('.//a[@class="biz-name"]//text()').extract())
            item["rating"] = float(" ".join(site.xpath('.//div[@class="rating-large"]/i/@title').extract())[0][:2])
            item["snippet"]=unicode("".join(site.xpath('.//p[@class="snippet"]/text()').extract()))
            item["category"] =" ".join(site.xpath('.//span[@class="category-str-list"]//text()').extract())
            item["url"] =baseUrl+" ".join(site.xpath('.//a[@class="biz-name"]/@href').extract())
            item["address"] = " ".join(site.xpath('.//address//text()').extract())
            item["review_count"]=int(site.xpath('.//span[@class="review-count rating-qualifier"]/text()').extract()[0].lstrip().partition(" ")[0])

            yield item

    def parse_reviews(self,response):
        sel = HtmlXPathSelector(response)
        reviews = sel.xpath('//div[@class="review review--with-sidebar"]')
        item=Reviews_item()

        for review in reviews:
            item['review_id']=" ".join(review.xpath('.//@data-review-id').extract())
            item['reviewer'] = " ".join(review.xpath('.//li[@class="user-name"]/a/text()').extract())
            item['review_rating'] = float(" ".join(review.xpath('.//meta[@itemprop="ratingValue"]/@content').extract()))
            item['review_text'] = " ".join(review.xpath('.//p[@itemprop="description"]/text()').extract())

            item['site_name'] = " ".join(sel.xpath('//a[@class="biz-name"]/text()').extract())
            item['site_id'] = " ".join(sel.xpath('//a[@class="biz-name"]/@data-hovercard-id').extract())
            item['site_rating'] =float(sel.xpath('//meta[@itemprop="ratingValue"]/@content').extract()[0])

            item['url'] = response.url
            #print item['review_id']
            yield item

