import nltk
import string
import os
from Search import getPlaceName
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.stem.porter import PorterStemmer
from sklearn.metrics.pairwise import linear_kernel
import operator

positive_path = 'H:/WareHouse/TREC/Contextual Suggestion/data/positive/'
negative_path = 'H:/WareHouse/TREC/Contextual Suggestion/data/negative/'

stemmer = PorterStemmer()
placeScore={}
def stem_tokens(tokens, stemmer):
    stemmed = []
    for item in tokens:
        stemmed.append(stemmer.stem(item))
    return stemmed

def tokenize(text):
    tokens = nltk.word_tokenize(text)
    stems = stem_tokens(tokens, stemmer)
    return stems





def candidateMatrix(path):
    token_dict = {}
    for subdir, dirs, files in os.walk(path):
        for file in files:
            file_path = subdir + os.path.sep + file
            #print file_path
            shakes = open(file_path, 'r')
            text = shakes.read()
            lowers = text.lower()
            no_punctuation = lowers.translate(None, string.punctuation)
            token_dict[file] = no_punctuation
    return token_dict

positive_token_dict=candidateMatrix(positive_path)
#print "huan hang"
negative_token_dict=candidateMatrix(negative_path)
#print positive_token_dict
#this can take some time
p_tfidf = TfidfVectorizer(tokenizer=tokenize, stop_words='english')
n_tfidf = TfidfVectorizer(tokenizer=tokenize, stop_words='english')

ptfs = p_tfidf.fit_transform(positive_token_dict.values())
ntfs= n_tfidf.fit_transform(negative_token_dict.values())
#print ptfs
p_cosine_similarities = linear_kernel(ptfs[26:27], ptfs).flatten()
p_related_docs_indices = p_cosine_similarities.argsort()[:-50:-1]
#print p_related_docs_indices
for i in p_related_docs_indices:
    p_filename=positive_token_dict.keys()[i]
    p_placeID=p_filename.replace("_positive.txt","")
    if p_placeID not in placeScore.keys():
        placeScore[p_placeID]=p_cosine_similarities[i]
    else:
        placeScore[p_placeID]=p_cosine_similarities[i]+placeScore[p_placeID]

    #print p_placeID
    #if p_placeID<>"" and p_placeID<>"profilePositive.txt":
        #p_name=getPlaceName(p_placeID)
        #print p_name," ",positive_token_dict.keys()[i]," score=",p_cosine_similarities[i]
    print positive_token_dict.keys()[i]," score=",p_cosine_similarities[i]

n_cosine_similarities = linear_kernel(ntfs[26:27], ntfs).flatten()
n_related_docs_indices = n_cosine_similarities.argsort()[:-50:-1]
#print n_related_docs_indices
for i in n_related_docs_indices:
    n_filename=negative_token_dict.keys()[i]
    n_placeID=n_filename.replace("_negative.txt","")
    if n_placeID not in placeScore.keys():
        placeScore[n_placeID]=n_cosine_similarities[i]
    else:
        placeScore[n_placeID]=n_cosine_similarities[i]-placeScore[n_placeID]

    #if n_placeID<>"" and n_placeID<>"profileNegative.txt":
        #n_name=getPlaceName(n_placeID)
        #print n_name," ",negative_token_dict.keys()[i],"score=",n_cosine_similarities[i]
    print " ",negative_token_dict.keys()[i],"score=",n_cosine_similarities[i]

print placeScore
sorted_rank = sorted(placeScore.items(), key=operator.itemgetter(1))
print sorted_rank