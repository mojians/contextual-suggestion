__author__ = 'Jian Mo'
from bs4 import BeautifulSoup as bs

import urllib
import json
import csv
import time
import sys



MySecret2 ="AIzaSyAYEaYWRLr_j3YArIkv6vZSaQKOWDRalKU"
MySecret3="AIzaSyDBvAeT_w32T6zVAMGn9rIiJ5O0YV__Oi8"
MySecret="AIzaSyAA5LVQDn81oExPaAz62NlggPrPeuafPLw"
GoogleUrl="https://maps.googleapis.com/maps/api/place/nearbysearch/json?"
DetailUrl="https://maps.googleapis.com/maps/api/place/details/json?"

def SearchBy(url,**kwargs):
    parameter={"key":MySecret}

    for key,value in kwargs.iteritems():
        parameter[key]=value
    #parameter={"location":location,"radius":radius,"types":types,"name":name,"sensor":sensor,"key":MySecret}
    paras=urllib.urlencode(parameter)
    requestUrl=url + paras
    print requestUrl
    result=urllib.urlopen(requestUrl)
    return result.read()

def PlaceDetatil(placeID):
    return SearchBy(DetailUrl,placeid=placeID)

def getReviews(placeID,opinionPositive=True):
    detail=PlaceDetatil(placeID)
    jsonDict=json.loads(detail)
    #print jsonDict
    try:
        detailDict=jsonDict[u"result"][u"reviews"]

        #print detailDict[0]["rating"]
        #print detailDict[1]
        reviewSummary=""
        for review in detailDict:
                if opinionPositive==True:
                    if review["rating"]>3:
                        reviewSummary=reviewSummary+" "+review["text"]
                elif opinionPositive==False:
                    if review["rating"]<3:
                        reviewSummary=reviewSummary+" "+review["text"]
        #print reviewSummary
        return reviewSummary
    except:
        print "no reviews"
        return ""

#print getReviews("ChIJN1t_tDeuEmsRUsoyG83frY4")

def buildProfiles():
    profileTxt="C:/Users/Hasee/Desktop/TREC/Contextual Suggestion/2014/dataset/user1_examples2014.csv"
    f = open(profileTxt, 'rt')
    positiveIDs=[]
    negativeIDs=[]
    try:
        reader = csv.DictReader(f)
        for row in reader:
            if int(row["rating"])<>-2:
                #print row
                #print int(row["rating"])
                if int(row["rating"])<2:
                    #print type(negativeIDs)
                    #print row["placeID"]
                    negativeIDs.append(row["placeID"])
                elif int(row["rating"])>2:
                    #print "positve"
                    positiveIDs.append(row["placeID"])
    finally:
        f.close()
        print "negativeIDs: ",negativeIDs
        print "positiveIDs: ",positiveIDs
        profileDict={"negativeIDs":negativeIDs,"positiveIDs":positiveIDs}
        return profileDict



def reviewText(idList,opinionPositive):
    reviewTxt=""
    for id in idList:
        try:
            reviewTxt=reviewTxt+getReviews(id,opinionPositive)
        except:
            print "no reviews"
            pass
    return reviewTxt

def getPlaceName(placeID):
    searchResult=SearchBy(DetailUrl,placeid=placeID)
    jsonDict=json.loads(searchResult)
    placeName=jsonDict["result"]["name"]
    time.sleep(2)
    return placeName

if __name__ == '__main__':
    userProfile=buildProfiles()
    negativeText=reviewText(userProfile["negativeIDs"],False).encode('ascii', 'ignore')
    positiveText=reviewText(userProfile["positiveIDs"],True).encode('ascii', 'ignore')
    with open("profileNegative.txt","w") as text:
        text.write(negativeText)
    with open("profilePositive.txt","w") as text:
        text.write(positiveText)




# SearchResult=SearchBy(GoogleUrl,location="-33.8670522,151.1957362",types="restaurant")
#
# jsonDict=json.loads(SearchResult)
# resultDict=jsonDict[u"results"][0]
# placeTypes=resultDict[u"types"]
# for key,value in jsonDict.iteritems():
#     print key,":",value
# print resultDict[u"results"][0]