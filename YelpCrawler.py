__author__ = 'Jian Mo'

from bs4 import BeautifulSoup as bs
import mechanize
from urllib import urlencode

br=mechanize.Browser()
br.set_handle_robots(False)
br.addheaders= [('User-agent', 'chrome')]


base_url="http://m.yelp.com/search?"
desc="Millennium Park"
location="chicago"
mapsize="902 567"
parameter={"find_desc":desc,"find_loc":location,"mapsize":mapsize}
paras=urlencode(parameter)
query=base_url+paras
#query="http://m.yelp.com/"+"search?find_desc="+desc%20park&find_loc=Qu%C3%A9bec%2C%20QC&mapsize=1085%2C499
html=br.open(query).read()
soup = bs(html)
results = soup.findAll('ul',attrs={'class':'ylist ylist-bordered search-results'})

print results